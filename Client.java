/**
 * Created by m.najafi on 10/19/2017.
 */

import java.util.Vector;

public class Client {

        public String nom;
        private Serveur serveur;
        String messages;
        public Client(String nom) {
            this.nom = nom;

        }
        public boolean se_connecter(Serveur serveur){
            this.serveur = serveur;
            return serveur.connecter(this);
        }
        public void envoyer(String message){
            serveur.diffuser(message);

        }

        public String recois(String message){
            messages = message;
            return messages;
        }

    }
}
