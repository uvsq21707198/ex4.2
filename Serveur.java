/**
 * Created by m.najafi on 10/19/2017.
 */

import java.util.Vector;

public class Serveur {
    Vector<Client> client_connecter;
    public Serveur(){

        client_connecter = new Vector<Client>();
    }
    public boolean connecter(Client name){
        client_connecter.add(name);
        return true;

    }

    public  void diffuser(String message) {
        for (int i = 0; i < client_connecter.size(); i++) {
            client_connecter.elementAt(i).recois(message);


        }
    }
}